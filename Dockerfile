FROM docker:20.10.7 as build

RUN apk add --no-cache \
      build-base~=0 \
      gcc~=10 \
      libffi-dev~=3 \
      musl-dev~=1 \
      python3~=3 \
      python3-dev~=3 \
      py3-cryptography~=3 \
      py3-pip~=20 \
  && pip3 install --no-cache-dir \
      "ansible==3.*" \
  && pip3 install --no-cache-dir --no-compile \
      "ansible-lint==5.*" \
      "molecule==3.*" \
      "molecule-docker==0.*"

FROM docker:20.10.7

ENV container docker

COPY --from=build /usr/bin/ansible /usr/bin/ansible
COPY --from=build /usr/bin/ansible-connection /usr/bin/ansible-connection
COPY --from=build /usr/bin/ansible-lint /usr/bin/ansible-lint
COPY --from=build /usr/bin/molecule /usr/bin/molecule
COPY --from=build /usr/lib/python3.8/site-packages/ /usr/lib/python3.8/site-packages/

# Patch needed to fix error - remove when possible
RUN rm /usr/lib/python3.8/site-packages/ruamel.yaml-0.17.9-py3.9-nspkg.pth

SHELL ["/bin/ash", "-eo", "pipefail", "-c"]
RUN apk --no-cache add \
      python3~=3 \
  && ln -sf ansible /usr/bin/ansible-config \
	&& ln -sf ansible /usr/bin/ansible-console \
	&& ln -sf ansible /usr/bin/ansible-doc \
	&& ln -sf ansible /usr/bin/ansible-galaxy \
	&& ln -sf ansible /usr/bin/ansible-inventory \
	&& ln -sf ansible /usr/bin/ansible-playbook \
	&& ln -sf ansible /usr/bin/ansible-pull \
	&& ln -sf ansible /usr/bin/ansible-test \
	&& ln -sf ansible /usr/bin/ansible-vault \
  && find /usr/lib/ -name '__pycache__' -print0 | xargs -0 -n1 rm -rf \
  && find /usr/lib/ -name '*.pyc' -print0 | xargs -0 -n1 rm -rf

WORKDIR /work

ARG BUILD_DATE
ARG REVISION
ARG VERSION

LABEL maintainer="Megabyte Labs <help@megabyte.space"
LABEL org.opencontainers.image.authors="Brian Zalewski <brian@megabyte.space>"
LABEL org.opencontainers.image.created=$BUILD_DATE
LABEL org.opencontainers.image.description="Node.js files/configurations that support the creation of Dockerfiles"
LABEL org.opencontainers.image.documentation="https://gitlab.com/megabyte-labs/dockerfile/ci-pipeline/molecule/-/blob/master/README.md"
LABEL org.opencontainers.image.licenses="MIT"
LABEL org.opencontainers.image.revision=$REVISION
LABEL org.opencontainers.image.source="https://gitlab.com/megabyte-labs/dockerfile/ci-pipeline/molecule.git"
LABEL org.opencontainers.image.url="https://megabyte.space"
LABEL org.opencontainers.image.vendor="Megabyte Labs"
LABEL org.opencontainers.image.version=$VERSION
LABEL space.megabyte.type="ci-pipeline"
